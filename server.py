
from flask import Flask, render_template, request, redirect, url_for
import sqlite3 as sql
import books
import collections

# import collections
# import yaml
#
# with open('conf.yaml') as f:
#     CONF = yaml.load(f, Loader=yaml.FullLoader)
#     print(CONF['theme'])


def createDB():
    conn = sql.connect('data.db')
    print("Opened database successfully")

    conn.execute('CREATE TABLE books (book_id INTEGER PRIMARY KEY, title TEXT, author TEXT, date YEAR, language TEXT, description TEXT, user TEXT, doctype TEXT, collection TEXT, icone TEXT)')
                # 'CREATE TABLE collections (collections_id INTEGER PRIMARY KEY, title TEXT, date YEAR, description TEXT, user TEXT, books TEXT)')
    print("data base est ")
    conn.close()

def createCollectionTab():
    conn = sql.connect('data.db')
    print("Opened database successfully")

    conn.execute('CREATE TABLE collections (collection_id INTEGER PRIMARY KEY, title TEXT, description TEXT, user TEXT, books TEXT)')
    print("table des collections est l")
    conn.close()

app = Flask(__name__)

bk = books.Books()
bk.dirFiles = 'files/'
bk.dbFile = 'data.db'
bk.champs = ['title', 'author', 'date', 'language', 'description', 'user', 'doctype', 'collection', 'icone']

# cc = collections.Collections()
# cc.dirFiles = 'files/'
# cc.dbFile = 'data.db'
# cc.champs = ['title', 'user', 'date', 'description', 'books']


@app.route('/create_db')
def create_db():
    try:
        createDB()
        return 'The database has been create;'
    except:
        return 'Error ! '

@app.route('/create_collection_tab')
def create_collection_tab():
    try:
        createCollectionTab()
        return 'collection tab create;'
    except:
        return 'Error ! '

@app.route('/reset_table/<table>')
def reset_table(table='test'):
    try:
        conn = sql.connect('data.db')
        c = conn.cursor()
        c.execute('DELETE FROM '+table+';',);
        msg = 'We have deleted', c.rowcount, 'records from the table.'
        conn.commit()
        conn.close()
        return msg
    except:
        return 'hello'

@app.route('/<pageType>')
def formulaire(pageType):
    if request.method == 'POST':
        if request.form['type'] == 'modify':
            row = bk.getBook(request.form['id'])
            # row = cc.getCollection(request.form['id'])
            print('###################')
            print('###################')
            return render_template(pageType+'.html', row=row)
    else:
        return render_template(pageType+'.html')

@app.route('/addrec', methods = ['POST'])
def addRec():
    if request.method == 'POST':
        msg = bk.add(request)
    return  msg[1] 

@app.route('/addcol', methods = ['POST'])
def addCol():

    if request.method == 'POST':
        msg = cc.add(request)
    return  msg[1] 


@app.route('/delete_book', methods = ['POST'])
def deleteBook():
    if request.method == 'POST':
        msg = bk.remove(request)
    return  msg[1] 

@app.route('/show_box', methods = ['GET', 'POST'])
def showBox():
    request.form.get('name')
    print(request.form['id'])
#    name = request.form.get("name")
     
@app.route('/list')
def list():
   rows = bk.getAllBooks()
#    rows = cc.getAllCollections()
   return render_template("list.html", rows=rows)

@app.route('/')
def page():
   rows = bk.getAllBooks()
#    rows = cc.getAllCollections()
   return render_template("index.html", rows=rows)


@app.route('/info')
def info():
#    request.form.get('name')
#    return render_template("info.html")
   rows = bk.getAllBooks()
   return render_template("info.html", rows=rows)



# fausses routes pour test css :
@app.route('/collection')
def collection():
   rows = bk.getAllBooks()
   return render_template("collection.html", rows=rows)

@app.route('/create_collection')
def create_collection():
#    rows = cc.getAllCollections()
   return render_template("createcollection.html")

@app.route('/chercher')
def chercher():
    rows = bk.getAllBooks()
    return render_template("chercher.html", rows=rows)

@app.route('/resultats')
def resultats():
   rows = bk.getAllBooks()
   return render_template("resultats.html", rows=rows)