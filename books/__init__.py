from flask import Flask, render_template, request, redirect, url_for
import sqlite3 as sql
import os
import logging

class Books:

    def __init__(self):
        self.dirFiles = ''
        self.dbFile = ''
        self.champs = []

    def addFolder(self, idBook):
        try:
            os.makedirs(self.dirFiles+str(idBook))
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
    
    def addFile(self, idBook, files):
        try:
            pass
        except:
            return 'error'
        
    def add(self, request):
        msg = []
        try:
            book = []
            book.append(request.form['title'])
            book.append(request.form['author'])
            book.append(request.form['date'])
            book.append(request.form['language'])
            book.append(request.form['description'])
            book.append(request.form['user'])
            book.append(request.form['doctype'])
            book.append(request.form['collection'])
            book.append(request.form['icone'])

            with sql.connect(self.dbFile) as con:
                cur = con.cursor()
                cur.execute(
                        """INSERT INTO books 
                        ("""+','.join(self.champs)+""")
                        VALUES (?,?,?,?,?,?,?,?,?)""",
                        (book))
                con.commit()
            try:
                book_id = cur.lastrowid
                self.addFolder(book_id)
                file = request.files['file']
                if file.filename == '':
                    flash('No selected file')
                    return redirect(request.url)
                else:
                    file.save(self.dirFiles+str(book_id)+'/'+file.filename)
                    return [0, file.filename]
                msg = ["0", "Record successfully added"]

            except:
                msg = ["1", "Problme de upload du fichier"]
                logging.exception('')
        except:
            con.rollback()
            msg = ["1", "error"]

        return msg

    def remove(self, request):
        try:
            with sql.connect(self.dbFile) as con:
                id_book = request.form["id"]
                sql_action = 'DELETE FROM books WHERE book_id=?'
                cur = con.cursor()
                cur.execute(sql_action, (id_book,))
            msg = ["0", "remove book "+str(id_book)+" !!"]
        except:
            msg = ["1", "error"]
        return msg 

    def modify(self, idBook):

        return 'modify book '+str(idBook)+' !!'
    
    def getAllBooks(self):
        con = sql.connect("data.db")
        con.row_factory = sql.Row
        cur = con.cursor()
        cur.execute("SELECT * FROM books")
        rows = cur.fetchall();
        return rows
    
    def getBook(self, idBook):
        con = sql.connect("data.db")
        con.row_factory = sql.Row
        cur = con.cursor()
        # cur.execute("SELECT * FROM books WHERE book_id=?", (idBook,))
        cur.execute("SELECT * FROM books WHERE book_id=?", (idBook,))
        row = cur.fetchall();
        return row
        
        # return 'get info book '+str(idBook)+' !!'

# class Collections: 

#     def __init__(self):
#         self.dirFiles = ''
#         self.dbFile = ''
#         self.champs = []

#     def addFolder(self, idCollection):
#         try:
#             os.makedirs(self.dirFiles+str(idCollection))
#         except OSError as e:
#             if e.errno != errno.EEXIST:
#                 raise
    
#     def addFile(self, idCollection, files):
#         try:
#             pass
#         except:
#             return 'error'
        
#     def add(self, request):
#         msg = []
#         try:
#             collection = []
#             collection.append(request.form['title'])
#             collection.append(request.form['user'])
#             collection.append(request.form['date'])
#             collection.append(request.form['description'])
     
#             with sql.connect(self.dbFile) as con:
#                 cur = con.cursor()
#                 cur.execute(
#                         """INSERT INTO collections 
#                         ("""+','.join(self.champs)+""")
#                         VALUES (?,?,?,?)""",
#                         (collection))
#                 con.commit()
#             try:
#                 collection_id = cur.lastrowid
#                 self.addFolder(collection_id)
#                 file = request.files['file']
#                 if file.filename == '':
#                     flash('No selected file')
#                     return redirect(request.url)
#                 else:

#                     file.save(self.dirFiles+str(collection_id)+'/'+file.filename)
#                     return [0, file.filename]

#                 msg = ["0", "Record successfully added"]

#             except:
#                 msg = ["1", "Problme de upload du fichier"]
#                 logging.exception('')
#         except:
#             con.rollback()
#             msg = ["1", "error"]

#         return msg

#     def getAllCollections(self):
#         con = sql.connect("data.db")
#         con.row_factory = sql.Row
#         cur = con.cursor()
#         cur.execute("SELECT * FROM collections")
#         rows = cur.fetchall();
#         return rows
    
#     def getCollection(self, idCollection):
#         con = sql.connect("data.db")
#         con.row_factory = sql.Row
#         cur = con.cursor()
#         # cur.execute("SELECT * FROM books WHERE book_id=?", (idBook,))
#         cur.execute("SELECT * FROM books WHERE book_id=?", (idCollection,))
#         row = cur.fetchall();
#         return row
        
